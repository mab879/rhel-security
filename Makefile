default:
	asciidoctor -v workshop/content/index.adoc -D public -a gitlab_hosted=true
	find ./public -type f -exec sed -i "s/%USERNAME%/\&lt;USERNAME\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%BASTION_IP_ADDRESS%/\&lt;BASTION_IP_ADDRESS\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%BASTION_PASSWORD%/\&lt;BASTION_PASSWORD\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%OPENSCAP_PASSWORD%/\&lt;OPENSCAP_PASSWORD\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%OPENSCAP_IP_ADDRESS%/\&lt;OPENSCAP_IP_ADDRESS\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%IDMSERVER_PASSWORD%/\&lt;IDMSERVER_PASSWORD\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%IDMSERVER_IP_ADDRESS%/\&lt;IDMSERVER_IP_ADDRESS\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%SESSIONRECORDING_IP_ADDRESS%/\&lt;SESSIONRECORDING_IP_ADDRESS\&gt;/g" {} \;
	find ./public -type f -exec sed -i "s/%GUID%/\&lt;GUID\&gt;/g" {} \;
	cp -rf workshop/content/images/ public/

